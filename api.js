
const fs = require('fs');

const login = (req, resp) => {
  resp.writeHead(200, { 'Content-Type': 'text/html' });
  fs.readFile('login.html', (err, data) => {
    resp.end(data);
  });
};
const register = (req, resp) => {
  resp.writeHead(200, { 'Content-Type': 'text/html' });
  fs.readFile('register.html', (err, data) => {
    resp.end(data);
  });
};

module.exports = {
  login,
  register,
};

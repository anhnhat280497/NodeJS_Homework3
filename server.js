
const express = require('express');

const apis = require('./api');

const app = express();
app.get('/', apis.login);
app.get('/register', apis.register);
const port = +process.env.PORT || 9999;

app.listen(port, (error) => {
  if (error) {
    console.error('run server got an error', error);
  } else console.log(`Server listening ${port}`);
});
